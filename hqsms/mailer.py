from django.core.mail import send_mail, send_mass_mail
from django.conf import settings
from .templates_old import open_table, end_table, get_table_row
from hqsms.models import ClientEmail


class Mailer(object):
    """ Classe responsável por enviar o e-mail com relatório final.
        Recebe uma lista de objetos com os dados do sms após envio"""

    def __init__(self, dest, data=None):
        """Inicia a instancia, recebe lista de objetos com dados dos smss enviados."""
        self.SUBJECT = settings.MAILER['SUBJECT']
        self.SENDER = settings.MAILER['SENDER']
        self.TO_EMAIL = dest
        print('Mailer.__init__.dest = ' + dest)
        print(self.TO_EMAIL)
        self.MESSAGE = settings.MAILER['MESSAGE']
        self.sms_data = data
        self.HTML_MESSAGE = open_table()
        for entry in self.sms_data:
            self.HTML_MESSAGE += get_table_row(entry)
        self.HTML_MESSAGE += end_table()
        self.send_report()
        del self

    def send_report(self):
        if self.TO_EMAIL is not None and len(self.TO_EMAIL) > 0:
            try:
                print(self.SUBJECT)
                # print(self.MESSAGE)
                print(self.SENDER)
                print(self.TO_EMAIL)
                # print(self.HTML_MESSAGE)
                result = send_mail(self.SUBJECT, self.MESSAGE, self.SENDER, [settings.MAILER['TO_EMAIL'], self.TO_EMAIL],
                          fail_silently=False, html_message=self.HTML_MESSAGE)
                print('result: ' + result.__str__())
            except Exception as e:
                print(e.__str__())
                raise e


class MassMailer:
    """Inicia a instancia, recebe lista de objetos com dados dos smss enviados."""

    def __init__(self):
        self.SUBJECT = 'Aviso de vencimento'
        self.SENDER = settings.MAILER['SENDER']
        self.MAIL_LIST = ClientEmail.objects.filter(status='P')
        self.send_client_mail()
        self.update_status()
        del self

    def send_client_mail(self):
            try:
                data_tuple = self.get_mail_tuple()
                result = send_mass_mail(data_tuple)
                print(data_tuple)
                print('result: ' + result.__str__())
            except Exception as e:
                print(e.__str__())
                raise e

    def get_mail_tuple(self):
        l = []
        for mail in self.MAIL_LIST:
            message = mail.message
            recipient = [mail.mail_address]
            l.append((self.SUBJECT, message, self.SENDER, recipient))
        data_tuple = tuple(l)
        return data_tuple

    def update_status(self):
        for mail in self.MAIL_LIST:
            mail.status = 'S'
            mail.save()
        return None
