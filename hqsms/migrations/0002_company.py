# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-19 03:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hqsms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('alias', models.CharField(max_length=4)),
                ('device', models.CharField(max_length=4)),
            ],
        ),
    ]
