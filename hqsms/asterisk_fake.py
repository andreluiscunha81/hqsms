

class manager:
    class Manager:
        HOST = None

        def connect(self, host):
            self.HOST = host
            return None

        def login(self, user, passwd):
            if not user or not passwd:
                raise manager.ManagerAuthException('Message')
            return None

        def send_action(self, action):
            if not action:
                raise manager.ManagerAuthException('Message')
            res = manager.Response('Message sent with Success')
            res.response.extend([action])
            return res

        def close(self):
            pass

    class ManagerException(Exception):
        strerror = ''

        def __int__(self):
            self.strerror = 'Error'

    class ManagerSocketException(ManagerException):
        pass

    class ManagerAuthException(ManagerException):
        pass

    class Response:
        response = []

        def __init__(self, message):
            self.response.append(message)

