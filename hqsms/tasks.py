from __future__ import absolute_import, unicode_literals
from celery import shared_task
from .models import Report, Sms, Company
from .khomp import Khomp
#from .khomp_fake import Khomp


@shared_task
def celery_sms():
    print('calling khomp to send sms')
    r = list(Report.objects.all().filter(status='P'))
    if r:
        s = Sms.objects.filter(report=r[0]).filter(status='P')
        if s:
            dev = Company.objects.filter(id=r[0].company_id).get().device
            print(dev)
            k = Khomp(r[0], s, dev)
            k.send_sms()
            celery_sms.delay()
    return 0
