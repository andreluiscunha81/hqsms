from django.db import models


class Report(models.Model):
    PENDING = 'P'
    SENT = 'S'
    ERROR = 'E'
    STATUS = (
        (PENDING, 'Pendente'),
        (SENT, 'Processado'),
        (ERROR, 'Não Processado'),
    )
    received_at = models.DateTimeField(auto_now_add=True)
    completed_at = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=1, choices=STATUS, default=PENDING,)
    company = models.ForeignKey('Company')
    email = models.EmailField()
    result = models.TextField(blank=True, null=True)

    def __str__(self):
        date = self.received_at.__str__()
        return str(self.pk) + ' ' + date


class Sms(models.Model):
    PENDING = 'P'
    SENT = 'S'
    ERROR = 'E'
    STATUS = (
        (PENDING, 'Pendente'),
        (SENT, 'Enviado'),
        (ERROR, 'Não Enviado'),
    )
    report = models.ForeignKey(Report)
    sent_at = models.DateTimeField(auto_now=True)
    cliente = models.CharField(max_length=50)
    dest = models.CharField(max_length=12, blank=True, null=True)
    message = models.TextField(max_length=160)
    status = models.CharField(max_length=1, choices=STATUS, default=PENDING,)
    result = models.CharField(max_length=12, blank=True, null=True)

    def __str__(self):
        return self.sent_at.__str__()


class ClientEmail(models.Model):
    PENDING = 'P'
    SENT = 'S'
    ERROR = 'E'
    STATUS = (
        (PENDING, 'Pendente'),
        (SENT, 'Enviado'),
        (ERROR, 'Não Enviado'),
    )
    report = models.ForeignKey(Report)
    sent_at = models.DateTimeField(auto_now=True)
    cliente = models.CharField(max_length=50)
    mail_address = models.EmailField()
    message = models.TextField(max_length=160)
    status = models.CharField(max_length=1, choices=STATUS, default=PENDING,)
    result = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.cliente


class Company(models.Model):
    name = models.CharField(max_length=50)
    alias = models.CharField(max_length=4)
    device = models.CharField(max_length=5)

    def __str__(self):
        return self.name

'''
class Message(models.Model):
    _SMS_='SMS'
    name = models.CharField(max_length=15)
'''