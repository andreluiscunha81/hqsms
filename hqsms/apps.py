from django.apps import AppConfig


class HqsmsConfig(AppConfig):
    name = 'hqsms'
