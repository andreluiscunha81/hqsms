from asterisk import manager
from django.conf import settings
from .mailer import Mailer


class Khomp(object):
    """ Classe responsável por enviar sms e chamar a classe de e-mail para enviar
    o relatório final. Recebe uma lista de objetos com os dados do sms"""

    def __init__(self, report=None, smslist=None, dev=None):
        """Inicia a instancia, recebe lista de objetos com dados dos SMSs."""
        self.ASTERISK_HOST = settings.ASTERISK_CONNECTION['HOST']
        self.ASTERISK_USER = settings.ASTERISK_CONNECTION['USER']
        self.ASTERISK_SECRET = settings.ASTERISK_CONNECTION['SECRET']
        self.KHOMP_DEV = ''
        self.valid_actions = []
        self.invalid_actions = []
        self.sms_list = smslist
        self.report = report
        self.KHOMP_DEV = dev
        if self.KHOMP_DEV is not None:
            if self.sms_list is not None:
                for sms in self.sms_list:
                    if sms.dest is not None:
                        self.valid_actions.append(self.getAction(sms))
                    else:
                        self.invalid_actions.append(self.getAction(sms))

    def getAction(self, sms):
        """Constrói a ação com os dados recebidos"""

        return {'Destination': sms.dest, 'Message': sms.message,
                'Action': 'KSendSMS', 'ActionID': sms.pk,
                'Device': self.KHOMP_DEV, 'res': ''}

    def send_sms(self):
        astmanager = manager.Manager()
        print('astmanager')
        try:
            try:
                astmanager.connect(self.ASTERISK_HOST)
                astmanager.login(self.ASTERISK_USER, self.ASTERISK_SECRET)
                if len(self.valid_actions) > 0:
                    for action in self.valid_actions:
                        result = astmanager.send_action(action)
                        action['res'] = result
                        action['res'] = action['res'].response[0].split()[-1]
                        action['res'] = 'Enviado' if action['res'] == 'Success' else 'Não enviado'
                        print('sent action {0} to number {1} with result {2}.'.format(
                            action['ActionID'], action['Destination'], action['res']))
                print("Sent: ", sum(action['res'] is not '' for action in self.valid_actions))
            except manager.ManagerSocketException as e:
                print("Error connecting to the manager: %s" % e.strerror)
            except manager.ManagerAuthException as e:
                print("Error logging in to the manager: %s" % e.strerror)
            except manager.ManagerException as e:
                print("Error: %s" % e.strerror)
        finally:
            astmanager.close()
            for sms in self.sms_list:
                action = next((i for i in self.valid_actions if i['ActionID'] == sms.pk), None)
                if action is not None:
                    sms.result = action['res']
                    sms.status = 'S'
                else:
                    sms.result = 'Inválido'
                    sms.status = 'E'
                sms.save()
            self.report.status = 'S'
            self.report.save()
            print('calling mailer')
            Mailer(self.report.email, self.sms_list)
            del astmanager
            return 0
