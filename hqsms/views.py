import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .tasks import celery_sms
from .smslist import Smslist
from celery.task.control import inspect
from .mailer import MassMailer

@csrf_exempt
def celery_envia(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode('UTF-8'))
        smslist = Smslist(mailto=data['tomail'], smslist=data['smslist'], sms_comp=data['sms_comp'])
        print(data['tomail'])
        if smslist.addToQueue():
            i = inspect()
            i = i.active()
            if len(i['worker@smsworker']) is 0:
                print('called task')
                celery_sms.delay()
                print('calling mass_mailer')
                MassMailer()
            else:
                print('ignored task')
            return JsonResponse({"response": '''O relatório foi recebido e está 
            sendo processado. O resultado será enviado ao e-mail fornecido quando
            o processo for concluído!'''})
        return JsonResponse({"response": "Erro ao adicionar a fila!"})
    return JsonResponse({"response": "Este método é inválido!"})
