from django.conf.urls import url

from . import views

urlpatterns = [
            url(r'^envia/$', views.celery_envia, name='celery_envia'),
]

