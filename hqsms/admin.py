from django.contrib import admin

from .models import Report, Sms, Company, ClientEmail

admin.site.register(Report)
admin.site.register(Sms)
admin.site.register(Company)
admin.site.register(ClientEmail)
# Register your models here.
