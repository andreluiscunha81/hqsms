from .models import Report, Sms, ClientEmail, Company


class Smslist(object):
    """ Classe de controle para inserir um relatorio a ser processado na db """

    '''Company alias to get the correct device'''

    def __init__(self, mailto=None, smslist=None, sms_comp=None):
        """Inicia a instancia, recebe lista de objetos com dados dos smss."""

        self.sms_list = smslist
        self.TO_EMAIL = mailto
        self.SMS_COMP = Company.objects.filter(alias=sms_comp).get()

    def createReport(self):
        """Constrói um relatorio com os dados recebidos"""
        return Report(email=self.TO_EMAIL, company=self.SMS_COMP)

    def createSms(self, sms, rep):
        """Constrói um sms com os dados recebidos"""
        d = sms['dest'] if sms['dest'] else None
        return Sms(report=rep,
                   cliente=sms['cliente'],
                   dest=d,
                   message=sms['message'])

    def create_client_email(self, sms, rep):
        """Constrói um email com os dados recebidos"""
        return ClientEmail(report=rep,
                           cliente=sms['cliente'],
                           mail_address=sms['client_email'],
                           message=sms['message'])

    def addToQueue(self):
        if self.sms_list is not None and self.TO_EMAIL is not None and self.SMS_COMP is not None:
            report = self.createReport()
            report.save()
            if report.pk:
                for sms in self.sms_list:
                    s = self.createSms(sms=sms, rep=report)
                    s.save()
                    print(sms['client_email'])
                    if sms['client_email'] != '':
                        cm = self.create_client_email(sms=sms, rep=report)
                        cm.save()
                return True
        return False
