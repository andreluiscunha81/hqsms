from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^(?P<sms_comp>\w+)/$', views.sms_select),
    url(r'^(?P<sms_comp>\w+)/single/$', views.sms_single),
    url(r'^(?P<sms_comp>\w+)/xml/$', views.sms_xml),
]
