/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global SimpleExcel */

var State = {
    data:'',
    toEmail:'',
    sms_comp:'',
    validMail: false,
    sent: false
};

function parse_xml(e){
    // parse as XML
    var file = e.target.files[0];
    var xmlParser = new SimpleExcel.Parser.XML();
    var conds = ['PI', 'PM', 'PS', 'PB', 'CH'];
    xmlParser.loadFile(file, function () {
        // draw HTML table based on sheet data
        var sheet = xmlParser.getSheet();
        //var table = document.getElementById('result');
        var smsObjs= [];
        sheet.forEach(function (el, i) {
            if ( (i>1) && (el.length>8 ) && (el.length <11)){ //original value is 9
                smsObjs.push({
                    cliente: el[0].value,
                    parcela: el[1].value,
                    parcelas: el[2].value,
                    titulo: el[3].value,
                    vencimento: el[4].value,
                    situacao: el[5].value,
                    fone1: el[6] ? el[6].value : "",
                    fone2: el[7] ? el[7].value : "",
                    condicao: el[8].value,
                    client_email: el.length === 10 ? el[9].value : "",
                    id: el[3].value
                });
            }
        });
        smsObjs = serialize(smsObjs);
        smsObjs = smsObjs.filter((el) => conds.indexOf(el.condicao) >= 0);
        smsObjs = smsObjs.map(smsFormatter);
        setState({ data: smsObjs});
    });
}

function serialize(arr) {
      let flat = [].concat(...arr);
      return flat.some(Array.isArray) ? serialize(flat) : flat;
}

function smsFormatter(obj, i) {
    var Company = State.sms_comp == 'JMJM'? 'Infra Eng.' : 'DOMI Urb.'
    let sms = {};
    due_str = FormatDate(obj.vencimento);
    sms.id = obj.id;
    sms.cliente = obj.cliente.split(' ');
    sms.cliente.pop();
    sms.cliente = sms.cliente.join(' ');
    sms.fone1 = foneFormatter(obj.fone1);
    sms.fone2 = foneFormatter(obj.fone2);
    sms.parcela = `${obj.parcela}/${obj.parcelas}`;
    sms.titulo = obj.titulo;
    sms.due = obj.vencimento;
    sms.late = Date.parse(sms.due.split('/').reverse().join('-')) < Date.now();
    sms.dest = validNumber(sms.fone1, sms.fone2);
    sms.client_email = testEmail(obj.client_email) ? obj.client_email : ''
    sms.message = sms.dest ? sms.late ?
        "Ola! Lembramos q/ consta em aberto sua parcela " + sms.parcela + " com vcto " + due_str +
        " ref ao imovel adquirido c/ a " + Company +", titulo "+ sms.titulo + ". Pague em dia e evite multas! Obrigado"
        : "Ola! Lembre-se q/ dia " + due_str + " vence sua parcela " + sms.parcela +
        " ref ao seu imovel adquirido com a " + Company +", titulo " + sms.titulo  +
        ". Pague em dia e evite multas! Obrigado"
        : false;
    return sms;
}

function foneFormatter(number) {
    /*if (number === '')
        return number;
    let formatted = number.split(' ');
    formatted[0] = formatted[0].replace(/[()]/g, '');
    formatted[1] = formatted[1].replace(/[-]/g, '');
    let firstDigit = Number(formatted[1].charAt(0));
    formatted[1] = firstDigit <= 9 && firstDigit >= 7 ?
        formatted[1].length === 9 ?
        formatted[1] :
        `9${formatted[1]}` :
        formatted[1];
    return formatted.join('');*/
    let formatted = number.replace(/\D/g,'');
    if (formatted.length<10){
        return null;
    }else{
        let ddd = formatted.substring(0,2);
        let fone = formatted.substring(2);
        if (fone.length === 9 ){
            if (fone.charAt(0)  <= 9 && fone.charAt(0) >= 7 ){
                return ddd+fone;
            }
        }else if (fone.length === 8){
            if (fone.charAt(0)  <= 9 && fone.charAt(0) >= 6){
                return `${ddd}9${fone}`
             }
        }
        return null;
    }
}

function validNumber(n1, n2) {
    let fd1 = n1 === '' ? 0 : Number(n1.substring(2).charAt(0));
    let fd2 = n2 === '' ? 0 : Number(n2.substring(2).charAt(0));

    return fd1 === 9 ? n1 : fd2 === 9 ? n2 : null;
}

function createSmsTb(SmsArr){
    var table = document.getElementById('result');
    var tbody = table.firstElementChild.nextElementSibling;
    tbody.innerHTML = '';
    //console.log(SmsArr);
    let i;
    for (i in SmsArr){
        var row = document.createElement('tr');
        let j;
        var sms = SmsArr[i];
        for (j in sms){
            //console.log("j="+j);
            if ((j==='cliente')||(j==='fone1')||(j==='client_email')||(j==='message')){
                    var cell = document.createElement('td');
                    if (sms[j]===false){
                        row.className = 'bg-danger';
                        cell.innerHTML = 'Não foram encontrados telefones válidos para este cliente. ' +
                        'A mensagem não será enviada. '+
                        'Por favor, insira um telefone válido para o cliente no sistema ates de exportar o relatório.'
                    } else {
                        if ((j==='cliente_email') && (sms[j] === '')){
                            cell.innerHTML = 'email inválido';}
                        else {cell.innerHTML = sms[j];}
                    }
                    console.log(sms[j]);
                    row.appendChild(cell);
            }
        }
        tbody.appendChild(row);
    }
    console.log(tbody);
}

function FormatDate(obj_venc){
    var due_str = '';
    var date = new Date(obj_venc);
    console.log(date);
    day = date.getDate().toString();
    if (day.length === 1)
        day = ''.concat('0', day);
    month = (date.getMonth()+1).toString();
    if (month.length === 1)
        month = ''.concat('0', month);
    year = (date.getFullYear()-2000).toString();
    due_str = day + '/' + month + '/' + year;
    console.log(due_str);
    return due_str;
}

function  handleEmail(e) {
    let email = e.target.value;
    if(testEmail(email)){
        setState({
            toEmail : email,
            validMail :  true
        });
    }else{
        setState({
        toEmail : '',
        validMail :  false
        });
    }
}

function  testEmail(email) {
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if(regex.test(email)){
        return true
    }else{
        return false
    }
}

function createJson(mailto, smslist, sms_comp){
    tmpObj = {'tomail': mailto,'smslist': smslist, 'sms_comp': sms_comp};
    json_result = JSON.stringify(tmpObj);
    return json_result;
}

function sendMessage(){
    var json_file = createJson(State.toEmail, State.data, State.sms_comp);
     console.log(json_file)
    console.log("Message sent.");
    $.ajax({
       type: "POST",
       url: "/api/envia/",
       data: json_file,
       success: function(json) {
           var response = JSON.parse(json);
            console.log(response);
           setState({sent: true, result: response.response});
       },
       // handle a non-successful response
       error : function(xhr,errmsg,err) {
           var msg = "Oops! We have encountered an error: "+errmsg; // add the error to the dom
           console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
       },
       dataType: "text",
       contentType : "text/plain"
   });
}

function validaTel(e){
    let telefone = e.target.value.replace(/\D/g,'');
    if(telefone.length === 11){
        setState({'tel': true});
        e.target.value = telefone;
    }
    else{
        setState({'tel': false});
    }
}

function createCustomSMS(msg){
    let smsObjs = [];
    smsObjs.push({
        cliente : 'Personalizado',
        dest : $('#telefone').val(),
        message : msg,
        client_email : '',
    });
    return smsObjs;
}

function hasContent(val){
    if (val===0){
    }
    else if(val===1){
        txtarea = $('#mensagem');
        txtHandler(txtarea);
    }
}

function updateCount(){
    var txt = $(this).val()
    var cs = txt.length;
    $('#characters').text(cs);
    // console.log(cs);
    // hasContent(txt)
}

function txtHandler(e){
    let msg = txtFormatter(e.target.value);
    let smsObj = createCustomSMS(msg);
    setState({data: smsObj});
}

function txtFormatter(txt){
    /* Funcionando */
    var tmp = '';
    tmp = txt;
    tmp = tmp.replace(/[ãáàâåä]/g,'a');
    tmp = tmp.replace(/[ÀÁÂÅÄÃ]/g,'A');
    tmp = tmp.replace(/[éèêë]/g,'e');
    tmp = tmp.replace(/[ÈÉÊË]/g,'E');
    tmp = tmp.replace(/[íìîï]/g,'i');
    tmp = tmp.replace(/[ÌÍÏÎ]/g,'I');
    tmp = tmp.replace(/[óòôöõ]/g,'o');
    tmp = tmp.replace(/[ÒÓÖÔÕ]/g,'O');
    tmp = tmp.replace(/[ùúûü]/g,'u');
    tmp = tmp.replace(/[ÙÚÜÛ]/g,'U');
    tmp = tmp.replace(/[ç]/g,'c');
    tmp = tmp.replace(/[Ç]/g,'C');
    return tmp;
}

function setState(obj){
    var el;
    for (el in obj){
       State[el] = obj[el];
    }
    applyState();
}

function changeAlertStatus(status, message){
    var new_class = "alert alert-".concat(status);
    $("#alert").attr( "class", new_class );
    $("#alert").html(message);
}

