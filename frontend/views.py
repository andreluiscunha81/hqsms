from django.shortcuts import render
from hqsms.models import Company


def home(request):
    comp_list = Company.objects.all()
    return render(request, 'home.html', {'comp_list': comp_list})


def sms_single(request, sms_comp=None):
    comp = Company.objects.get(alias=sms_comp)
    return render(request, 'sms_single.html', {'comp': comp})


def sms_xml(request, sms_comp=None):
    comp = Company.objects.get(alias=sms_comp)
    return render(request, 'sms_xml.html', {'comp': comp})


def sms_select(request, sms_comp=None):
    comp = Company.objects.get(alias=sms_comp)
    return render(request, 'sms_select.html', {'comp': comp})
